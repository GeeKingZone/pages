var store = [{
        "title": "000 Gee King Zone - ¿Cómo hacemos un Podcast?",
        "excerpt":"Anchor, Codeberg, Protonmail, Markdown y que no se nos olvide hacer un logo con Canva. Episodio piloto en el que grabamos por primera vez. Musica con licencias Creative Commons: Swan Song by Paper Navy (via freemusicarchive.org) African Bliss by John Bartmann (via freemusicarchive.org) Man Outa Town by Forget the Whale...","categories": ["episodios","podcast"],
        "tags": [],
        "url": "/episodios/podcast/2021/04/25/000-como-hacemos-un-podcast/",
        "teaser": "/assets/images/portada.jpg"
      },{
        "title": "001 Gee King Zone - Gatillazo",
        "excerpt":"Primer episodio oficial. Empezamos nuestra andadura oficial con una dedicatoria muy especial. Seguimos hablando sobre el origen del nombre y de cómo pronunciarlo. Mal hemos empezado… Enlanzamos después con el episodio 000 hablando principalmente de ProtonMail, rastreo en las Apps, veracrypt, passwordsafe, bitwarden, openvpn, wireguard, el peaje de ser tu...","categories": ["episodios","podcast"],
        "tags": [],
        "url": "/episodios/podcast/2021/05/07/001-gatillazo/",
        "teaser": "/assets/images/portada.jpg"
      },{
        "title": "002 - Gee King Zone - The Social Dilemma",
        "excerpt":"¡Segundo episodio!. Compartimos nuestras conclusiones sobre el documental ‘The Social Dilemma’, del que extraemos los que para nosotros son los temas principales: dependencia y monetización, polarización de la sociedad (modelos de publicidad), el mundo dirigido por algoritmos y una remota amenaza existencial. Por fin nos acordamos de presentarnos y despedirnos...","categories": ["episodios","podcast"],
        "tags": [],
        "url": "/episodios/podcast/2021/05/22/002-the-social-dilemma/",
        "teaser": "/assets/images/portada.jpg"
      },{
        "title": "003 - Gee King Zone - Geolocalización y navegación segura",
        "excerpt":"¡Tercer episodio y consolidando nuestro estilo! En esta ocasión queremos traer a nuestra tertulia los servicios de Red Privada Virtual o VPN en sus siglas en inglés. Comentamos qué son y qué utilidades se pueden encontrar para su uso particular. Comentamos algunas soluciones comerciales: TunnelBear, con base en Toronto, Canada...","categories": ["episodios","podcast"],
        "tags": [],
        "url": "/episodios/podcast/2021/06/07/003-geolocalizacion-y-navegacion-segura/",
        "teaser": "/assets/images/portada.jpg"
      },{
        "title": "004 - Gee King Zone - Metapodcast",
        "excerpt":"Se ha hecho esperar, ¡pero aquí está el cuarto episodio! Esta vez hacemos un metapodcast para compartir con vosotros nuestras opiniones sobre esta andadura. Explicamos el motivo del ¡YA! inicial, cómo y cuándo encontramos hueco para grabar, alguna anécdota curiosa.. ¡Muchas gracias por estar ahí, esperamos que os guste! Música...","categories": ["episodios","podcast"],
        "tags": [],
        "url": "/episodios/podcast/2021/07/09/004-Metapodcast/",
        "teaser": "/assets/images/portada.jpg"
      },{
        "title": "005 Gee King Zone - Cual era mi password",
        "excerpt":"¡Empezamos segunda temporada! Gracias a todos por volveros a pasar por aquí. Hoy hablamos de contraseñas, y de nuestra manera de gestionarlas. Esperamos que os ayude y os sea interesante. Mencionamos la búsqueda en google con el modo Hacker y Klingon. Aquí tenéis los enlaces: Hacker: https://www.google.com/?hl=xx-hacker Klingon: https://www.google.com/?hl=tlh-ES ¡Muchas...","categories": ["episodios","podcast"],
        "tags": [],
        "url": "/episodios/podcast/2021/09/11/005-cual-era-mi-password/",
        "teaser": "/assets/images/portada.jpg"
      },{
        "title": "006 - Gee King Zone - Pago Seguro en Internet",
        "excerpt":"¡Ya tenemos nuevo episodio! Gracias a todos por volveros a pasar por aquí. Hoy os agradecemos vuestras sugerencias, recogemos el guante con uno de los temas y hablamos de cómo hacemos para pagar seguro en Internet. Esperamos que os ayude y os sea interesante. ¡Muchas gracias por estar ahí! Música...","categories": ["episodios","podcast"],
        "tags": [],
        "url": "/episodios/podcast/2021/10/12/006-Pago-Seguro-en-Internet/",
        "teaser": "/assets/images/portada.jpg"
      },{
        "title": "007 - Gee King Zone - Juegos de Guerra (War Games)",
        "excerpt":"¡GOLF-ECHO-ECHO KILO-INDIA-NOVIEMBRE-GOLF ZULU-OSCAR-NOVIEMBRE-ECHO! Hoy hablamos de Juegos de Guerra. Sobre lo que significó para toda una generación, sobre nuestras sensaciones al volver a verla, nuestras reflexiones, sobre el empoderamiento de la mujer… ¡Y TENEMOS TRIVIAL! Enlaces comentados durante el episodio: Should a self-driving car kill the baby or the grandma?...","categories": ["episodios","podcast"],
        "tags": [],
        "url": "/episodios/podcast/2021/11/04/007-Juegos-de-guerra-copy/",
        "teaser": "/assets/images/portada.jpg"
      },{
        "title": "008 Gee King Zone - 10 Servicios esenciales con o sin Docker",
        "excerpt":"Servicios útiles Gracias a todos por volveros a pasar por aquí. Hoy os os hablamos de Servicios útiles para nosotros y que os pueden aportar algún valor. Esperamos que os ayude y os sea interesante. Aquí os dejamos la lista de servicios y enlaces de los que hemos hablado: backup...","categories": ["episodios","podcast"],
        "tags": [],
        "url": "/episodios/podcast/2021/12/14/006-10-servicios-esenciales-copy/",
        "teaser": "/assets/images/portada.jpg"
      },{
        "title": "Gee King Zico",
        "excerpt":"Con nuestros mejores deseos para todos vosotros, os deseamos una Feliz Navidad!!!  😉    ","categories": ["episodios","podcast"],
        "tags": [],
        "url": "/episodios/podcast/2021/12/25/007-Gee-king-zico-copy/",
        "teaser": "/assets/images/portada.jpg"
      },{
        "title": "009 Gee King Zone - Jam Session",
        "excerpt":"Micros nuevos. Whisky con hielos. Y a darle al REC. No es un Haiku, es nuestra despedida de 2021 y nuestro gran agradecimiento por estar ahí y escucharnos. Gracias y más gracias!!! Que tengáis una buena salida de año y ¡feliz año nuevo 2022 a tod@s! Hoy echamos un vistazo...","categories": ["episodios","podcast"],
        "tags": [],
        "url": "/episodios/podcast/2021/12/31/008-Jam-Session-copy/",
        "teaser": "/assets/images/portada.jpg"
      },{
        "title": "Toma falsa",
        "excerpt":"Música con licencias Creative Commons:      Swan Song by Paper Navy (via freemusicarchive.org)    ","categories": ["episodios","podcast"],
        "tags": [],
        "url": "/episodios/podcast/2021/12/31/Toma-falsa/",
        "teaser": "/assets/images/portada.jpg"
      },{
        "title": "Private Relay",
        "excerpt":"n nuevo episodio para todos Uds. donde traemos a la palestra el servicio Private Relay de Apple. Explicamos qué es y cómo funciona (al menos lo intentamos. Saludos, Sr. Smith 🤣) Es un servicio con sus luces y sombras, pero no tan malo como lo pintan las operadoras europeas, según...","categories": ["episodios","podcast"],
        "tags": [],
        "url": "/episodios/podcast/2022/01/10/private-relay/",
        "teaser": "/assets/images/portada.jpg"
      },{
        "title": "El metodo de los sobres",
        "excerpt":"En este episodio nos lanzamos a charlar sobre economía doméstica. Comentamos nuestras experiencias en la gestión de nuestras finanzas personales y cómo conseguir nuestras metas. Comentamos aplicaciones como GNUCash y MoneyDance. Charlamos sobre el método de los sobres y cómo poder gestionarlo con la ayuda de la aplicación Buckets. https://www.budgetwithbuckets.com...","categories": ["episodios","podcast"],
        "tags": [],
        "url": "/episodios/podcast/2022/02/09/metodo-sobres/",
        "teaser": "/assets/images/portada.jpg"
      },{
        "title": "Navegando con GPS y sin retrovisores",
        "excerpt":"Tras demasiado tiempo sin grabar, nos embarcamos en un nuevo episodio y charlamos sobre navegadores. Hemos llegado a las 500 descargas, ¡¡¡¡Muchas gracias!!!! Actualizamos el formato del podcast con una sección de noticias recientes relacionadas de alguna manera con temas que ya hemos abordado. EL COMENTARIO DE LA NOTICIA En...","categories": ["episodios","podcast"],
        "tags": [],
        "url": "/episodios/podcast/2022/03/28/navegando-gps/",
        "teaser": "/assets/images/portada.jpg"
      }]
